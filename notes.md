---
date: 05 Dec 2021
title: Indexing and Searching with Apache Lucene
subtitle: "Labo 3: Lucene"
lang: en
author:
- Roosembert Palacios
- Florian Gazetta
---

## Understanding the Lucene API

- What are the types of the fields in the index?

  By looking at the implementation of `indexDoc`, the function that does the
  index for a single document, we get hinted at some of the fields that are
  used by this particular implementation; in particular `StringField`,
  `LongPoint`, and `TextField`. By looking at the _javadoc_ of their common
  superclass (`org.apache.lucene.document.Field`) we can identify the following
  subclasses.

  - `TextField`: Reader or String indexed for full-text search
  - `StringField`: String indexed verbatim as a single token
  - `IntPoint`: int indexed for exact/range queries.
  - `LongPoint`: long indexed for exact/range queries.
  - `FloatPoint`: float indexed for exact/range queries.
  - `DoublePoint`: double indexed for exact/range queries.
  - `SortedDocValuesField`: byte[] indexed column-wise for sorting/faceting
  - `SortedSetDocValuesField`: `SortedSet<byte[]>` indexed column-wise for
    sorting/faceting.
  - `NumericDocValuesField`: long indexed column-wise for sorting/faceting
  - `SortedNumericDocValuesField`: `SortedSet<long>` indexed column-wise for
    sorting/faceting.
  - `StoredField`: Stored-only value for retrieving in summary results

- What are the characteristics of each type of field in terms of indexing,
  storage and tokenisation.

  - `TextField`: Indexed and tokenized, without term vectors, not stored by
    default.
  - `StringField`: Indexed but not tokenized, not stored by default.
  - `IntPoint`: An indexed int field for fast range filters, not tokenized
    (not a String), not stored.
  - `LongPoint`: An indexed long field for fast range filters, not tokenized
    (not a String), not stored.
  - `FloatPoint`: An indexed float field for fast range filters, not tokenized
    (not a String), not stored.
  - `DoublePoint`: An indexed double field for fast range filters, not tokenized
    (not a String), not stored.
  - `SortedDocValuesField`: Field that stores a per-document `BytesRef` value,
    indexed for sorting. The original reference is tokenized but not stored.
  - `SortedSetDocValuesField`: Field that stores a set of per-document,
    `BytesRef` values, indexed for faceting, grouping or joining.
    The original reference is tokenized but not stored.
  - `NumericDocValuesField`: Field that stores a per-document long value for
    scoring, sorting or value retrieval. Not tokenized (not a String), not
    stored.
  - `SortedNumericDocValuesField`: Field that stores a per-document long values
    for scoring, sorting or value retrieval. Not tokenized (not a String), not
    stored.
  - `StoredField`: A field whose value is stored so that `IndexSearcher.doc` and
    `IndexReader.document()` will return the field and its value. The value is
    tokenized.

- Does the command line demo use _stopword removal_?

  From the _Demo API_ documentation one could think they do:

  > The Analyzer we are using is `StandardAnalyzer`, which creates tokens using
  > the Word Break rules from the Unicode Text Segmentation algorithm specified
  > in Unicode Standard Annex #29; converts tokens to lowercase; and then
  > filters out *stopwords*.

  After running some manual experiments, one can notice that the queries
  “_adventures huckleberry finn_” and “_the adventures of huckleberry finn_”
  produce a different number of results, which suggests that no _stopwords_ are
  being removed.

  By looking in the `SearchFiles` source, we can verify this:
  Indeed, the default set of _stopwords_ in the `StandardAnalyzer` is empty.

- Does the command line demo use _stemming_?

  The _Demo API_ documentation does not mention anything about stemming.
  After running some experiments, we witness that the queries “_compile_” and
  “_compiling_” produce a different amount of results, which suggests that no
  stemming was performed.

  By looking at the `StandardAnalyzer` implementation, we notice that the
  token stream initialization and normalization step of the analyser do only
  perform a lowercase transformation.
  By contrast, looking at the implementation of the `EnglishAnalyzer` we notice
  that the token stream initialization does include the stopword filter and
  stemming (`PorterStemFilter`) on top of the lowercase filter.

- Is the search of the command line demo case insensitive.

  Yes. A quick test reveals the number of results for queries “_Lucene_” and
  “_lucene_” matches, this is not a proof but a good indicator.
  This is confirmed by previous answers sine the `QueryParser` used by the
  `SearchFiles` implementation uses the `StandardAnalyzer`, which in turn does
  configure the `LowerCaseFilter` in the token stream initialization and
  normalization.

- Does it matter whether stemming occurs before or after _stopword_ removal?

  Yes. For instance word 'noted' would be stemmed into word 'not', which would
  be filtered by the _stopwords_ filter and thus not be considered.
  This is undesirable since 'noted' is not a stop word.

## Indexing {.pagebreak}

### Find out what is a “term vector” in _Lucene_ vocabulary

In the Lucene's javadoc, _term vector_ is defined as “a list of the document's
terms and their number of occurrences in that document.”.
A term is the basic searchable unit in Lucene; it is always associated to a
field.
Though the above definition hints at terms being part of the document, rather
they belong to a field, thus many terms may contain the same text if they are
associated to different fields.

### What should be added ot the code to have access to the term vector in the index?

Since terms belong to fields, the field type needs to be set to store term
vectors (e.g. call `setStoreTermVectors(true)`).
Storing term vectors is disabled by default.
Built-in types provide no mechanism to store term vectors, on should thus use
custom `FieldType`s since built-in types are not extensible.

### Compare the size of the index before and after enabling “term vector”

Before:

Number of documents: 3202
Number of terms: 15489
Size: 1340k

After:

Number of documents: 3202
Number of terms: 15489
Size: 2036k

As expected, enabling term vectors does not change the number of documents or
terms, but only the size of the index due to the additional information being
stored per field (author, title and summary).

Mind that the _id_ field cannot store term vectors since it is not indexed.

## Using different Analyzers {.pagebreak}

### Index the publication list using each of the following analyzers

- WhitespaceAnalyzer
  Uses `WhitespaceTokenizer`: Divides text at whitespace characters.

  Indexed documents: 3202, indexed terms: 23312 (summary: 15655).
  Top terms of the summary field: of, the, is, and, a, to, in, for, The, are.
  Size: 2184k.
  Time indexing: 1818ms.

- EnglishAnalyzer
  Uses `StandardTokenizer`, which implements the word break rules from the
  unicode text segmentation algorithm.
  It also removes english possessives from words, normalizes text to lowercase,
  removes common English words that are not usually useful for searching and
  finally performs stemming using _Porter's algorithm_.
  Some words may also be added to this filter to preserve them from
  transformations such as stemming.

  Indexed documents: 3202, indexed terms: 11144 (summary: 5265).
  Top terms of the summary field: which, us, comput, program, system, present,
  describ, paper, can, method.
  Size: 1716k.
  Time indexing: 1007ms.

- ShingleAnalyzerWrapper (using shingle size 1 and 2)
  This analyzer wraps another analyzer and applies shingles of a given size,
  that is, adds groupings of a number of adjacent words to the tokenstream.
  Since using a shingle size of one is equivalent to identity, we set the
  shingle size to two.

  By default, this wraps the StandardAnalyzer.
  Indexed documents: 3202, indexed terms: 97028 (summary: 78608).
  Top terms of the summary field: the, of, a is, and, to, in, for, are, of the.
  Size: 4124k.
  Time indexing: 4207ms.

  Additional run using the EnglishAnalyzer:
  Indexed documents: 3202, indexed terms: 62938 (summary: 48107).
  Top terms of the summary field: which, us, comput, program, `us _`, system
  present, describ, `_ us`, paper.
  Size: 3232k.
  Time indexing: 1908ms.

- ShingleAnalyzerWrapper (using shingle size 1 and 3, but not 2)
  This analyzer wraps another analyzer and applies shingles of a given size,
  that is, adds groupings of a number of adjacent words to the tokenstream.
  Since using a shingle size of one is equivalent to identity, we set the
  shingle size to three.

  By default, this wraps the StandardAnalyzer.
  Indexed documents: 3202, indexed terms: 150616 (summary: 130185).
  Top terms of the summary field: the, of, a is, and, to, in, for, are, this.
  Size: 5448k.
  Time indexing: 2027ms.

  Additional run using the EnglishAnalyzer:
  Indexed documents: 3202, indexed terms: 116508 (summary: 98354).
  Top terms of the summary field: which, us, comput, program, system, present,
  describ, paper, can, gener.
  Size: 4308k.
  Time indexing: 2510ms.

- StopAnalyzer
  This analyzer removes stop words from the token stream and normalizes text
  to lowercase.

  Indexed documents: 3202, indexed terms: 13874 (summary: 7932).
  Top terms of the summary field: computer, system, paper, presented, time,
  program, data, method, algorithm, discussed.
  Size: 1732k.
  Time indexing: 852ms.

Conclusions:

- Common English words (thus unuseful) should be filtered out from text sources.
  The waste space (and a marginal amount of processing time) while not being
  useful for most searches.

- Pertinence (and hence accuracy) of terms may be increased by discarding
  irrelevant terms off the token stream, this is the cheapest (resource-wise)
  method of increasing accuracy.

- While shingling may seem like a good idea to further increase the pertinence
  of results, this is more expensive both computationally and storage-wise than
  other approaches.

## Reading Index {.pagebreak}

> Note: The field 'authors' was renamed to 'author' to match our field names.

- What is the author with the highest number of publications?

  `Thacher Jr., H. C.` with 38 publications.

- List the top 10 terms in the title field with their frequency.

  - algorithm: 975
  - computer: 275
  - system: 178
  - programming: 158
  - method: 136
  - data: 115
  - systems: 116
  - language: 104
  - program: 97
  - matrix: 91

### Code for section 3.3

```java
public void printTopRankingTerms(String field, int numTerms) {
  var cmp = new HighFreqTerms.TotalTermFreqComparator();
  try {
    var terms = HighFreqTerms.getHighFreqTerms(indexReader, 10, field, cmp);
    System.out.print("Top ranking terms for field ["  + field +"] are: ");
    System.out.println(Arrays.toString(Stream.of(terms)
      .map(t -> t.termtext.utf8ToString() + ": " + Integer.toString(t.docFreq))
      .toArray()));
  } catch (Exception e) {
    throw new RuntimeException("Could not calculate top ranking terms.", e);
  }
}
```

## Searching {.pagebreak}

### Code for section 3.4

```java
public TopDocs query(String q) {
  System.out.println("- Searching for [" + q +"]");

  QueryParser parser = new QueryParser("summary", this.analyzer);
  Query query;
  try {
    query = parser.parse(q);
  } catch (Exception e) {
    throw new RuntimeException("Failed to parse query.", e);
  }
  try {
    return this.indexSearcher.search(query, 10);
  } catch (IOException e) {
    throw new RuntimeException("Could not execute search.", e);
  }
}

public void displayResults(TopDocs results) {
  long nhits = results.totalHits.value;
  System.out.println("Found " + Long.toString(nhits) + " hits.");
  for (ScoreDoc r : results.scoreDocs) {
    Document doc;
    try {
      doc = queriesPerformer.getIndexSearcher().doc(r.doc);
    } catch (IOException e) {
      throw new RuntimeException("Could not retrieve result document.", e);
    }
    var idBytes = doc.getField("id").binaryValue().bytes;
    String id = Long.toString(LongPoint.decodeDimension(idBytes, 0));
    String title = doc.get("title");
    String score = Float.toString(r.score);
    System.out.println(id + ": " + title + " (" + score + ")");
  }
}
```

### Queries

#### Searching for `compiler program`

```plain
Found 679 hits.
3189: An Algebraic Compiler for the FORTRAN Assembly Program (1.400693)
1215: Some Techniques Used in the ALCOR ILLINOIS 7090 (1.3382424)
1988: A Formalism for Translator Interactions (1.2983286)
799: Design of a Separable Transition-Diagram Compiler* (1.2843311)
1647: WATFOR-The University of Waterloo FORTRAN IV Compiler (1.2778773)
1122: A Note on Some Compiling Algorithms (1.2518111)
1183: A Note on the Use of a Digital Computer for Doing Tedious Algebra and Programming (1.2494278)
1459: Requirements for Real-Time Languages (1.2196473)
637: A NELIAC-Generated 7090-1401 Compiler (1.1973245)
46: Multiprogramming STRETCH: Feasibility Considerations (1.193163)
```

#### Searching for `"Information Retrieval"`

```plain
Found 20 hits.
891: Everyman's Information Retrieval System (1.5950764)
1935: Randomized Binary Search Technique (1.4266797)
2288: File Organization: The Consecutive Retrieval Property (1.3989745)
1457: Data Manipulation and Programming Problems in Automatic Information Retrieval (1.3023745)
655: COMIT as an IR Language (1.2811962)
1699: Experimental Evaluation of Information Retrieval Through a Teletypewriter (1.1888998)
1514: On the Expected Gain From Adjust ing Matched Term Retrieval Systems (1.1007073)
1627: Application of Level Changing to a Multilevel Storage Organization (1.0754004)
2519: On the Problem of Communicating Complex Information (1.0088149)
2650: Order-n Correction for Regular Languages (1.0088149)
```

#### Searching for `Information AND Retrieval`

```plain
Found 36 hits.
2288: File Organization: The Consecutive Retrieval Property (1.9864943)
891: Everyman's Information Retrieval System (1.7366376)
1457: Data Manipulation and Programming Problems in Automatic Information Retrieval (1.7002742)
3134: The Use of Normal Multiplication Tables for Information Storage and Retrieval (1.5496716)
1032: Theoretical Considerations in Information Retrieval Systems (1.5208457)
1699: Experimental Evaluation of Information Retrieval Through a Teletypewriter (1.4560988)
1935: Randomized Binary Search Technique (1.4266797)
1514: On the Expected Gain From Adjust ing Matched Term Retrieval Systems (1.3622651)
2519: On the Problem of Communicating Complex Information (1.3236521)
655: COMIT as an IR Language (1.2811962)
```

#### Searching for `+Retrieval Information -Database`

```plain
Found 69 hits.
2288: File Organization: The Consecutive Retrieval Property (1.9864943)
891: Everyman's Information Retrieval System (1.7366376)
1457: Data Manipulation and Programming Problems in Automatic Information Retrieval (1.7002742)
3134: The Use of Normal Multiplication Tables for Information Storage and Retrieval (1.5496716)
1032: Theoretical Considerations in Information Retrieval Systems (1.5208457)
1699: Experimental Evaluation of Information Retrieval Through a Teletypewriter (1.4560988)
1935: Randomized Binary Search Technique (1.4266797)
1514: On the Expected Gain From Adjust ing Matched Term Retrieval Systems (1.3622651)
2519: On the Problem of Communicating Complex Information (1.3236521)
655: COMIT as an IR Language (1.2811962)
```

#### Searching for `Info*`

```plain
Found 205 hits.
222: Coding Isomorphisms (1.0)
272: A Storage Allocation Scheme for ALGOL 60 (1.0)
396: Automation of Program  Debugging (1.0)
397: A Card Format for Reference Files in Information Processing (1.0)
409: CL-1, An Environment for a Compiler (1.0)
440: Record Linkage (1.0)
483: On the Nonexistence of a Phrase Structure Grammar for ALGOL 60 (1.0)
616: An Information Algebra - Phase I Report-Language Structure Group of the CODASYL Development Committee (1.0)
644: A String Language for Symbol Manipulation Based on ALGOL 60 (1.0)
655: COMIT as an IR Language (1.0)
```

### Searching for `+"Information Retrieval"~5`

```plain
Found 30 hits.
891: Everyman's Information Retrieval System (1.5950764)
1935: Randomized Binary Search Technique (1.4266797)
2288: File Organization: The Consecutive Retrieval Property (1.3989745)
1457: Data Manipulation and Programming Problems in Automatic Information Retrieval (1.3023745)
655: COMIT as an IR Language (1.2811962)
1699: Experimental Evaluation of Information Retrieval Through a Teletypewriter (1.1888998)
1514: On the Expected Gain From Adjust ing Matched Term Retrieval Systems (1.1007073)
1627: Application of Level Changing to a Multilevel Storage Organization (1.0754004)
2519: On the Problem of Communicating Complex Information (1.0088149)
2650: Order-n Correction for Regular Languages (1.0088149)
```

## Tuning the Lucene Score

> Queries were executed on the summary field as specified in section 3.4.
> This section did not specify a domain for the search.

### Code for section 3.5

```java
public class MySimilarity extends ClassicSimilarity {
  @Override
  public float tf(float freq) {
    Double res = 1 + Math.log(freq);
    return res.floatValue();
  }

  @Override
  public float lengthNorm(int numTerms) {
    return 1f;
  }

  @Override
  public Explanation idfExplain(CollectionStatistics collectionStats,
      TermStatistics termStats) {
    final long df = termStats.docFreq();
    final long docCount = collectionStats.docCount();
    final float idf = idf(df, docCount);
    return Explanation.match(idf,
      "idf, computed as log(nDocs/(docFreq+1)) + 1 from:",
      Explanation.match(df, "docFreq, number of documents containing term"),
      Explanation.match(docCount, "nDocs, number of documents with field"));
  }

  @Override
  public float idf(long docFreq, long docCount) {
    Double frac = (double) docCount / (double)(docFreq + 1);
    Double r = 1d + Math.log(frac);
    return r.floatValue();
  }
}
```

### Results using `ClassicSimilarity`

```plain
Processing time ms: 1103
...
Searching for [compiler program]
Found 679 hits.
3189: An Algebraic Compiler for the FORTRAN Assembly Program (1.400693)
1215: Some Techniques Used in the ALCOR ILLINOIS 7090 (1.3382424)
1988: A Formalism for Translator Interactions (1.2983286)
799: Design of a Separable Transition-Diagram Compiler* (1.2843311)
1647: WATFOR-The University of Waterloo FORTRAN IV Compiler (1.2778773)
1122: A Note on Some Compiling Algorithms (1.2518111)
1183: A Note on the Use of a Digital Computer for Doing Tedious Algebra and Programming (1.2494278)
1459: Requirements for Real-Time Languages (1.2196473)
637: A NELIAC-Generated 7090-1401 Compiler (1.1973245)
46: Multiprogramming STRETCH: Feasibility Considerations (1.193163)
```

### Results using custom similarity class

```plain
Processing time ms: 2015
...
Searching for [compiler program]
Found 679 hits.
1647: WATFOR-The University of Waterloo FORTRAN IV Compiler (13.897842)
637: A NELIAC-Generated 7090-1401 Compiler (13.25242)
2534: Design and Implementation of a Diagnostic Compiler for PL/I (12.476707)
2923: High-Level Data Flow Analysis (12.421366)
2423: A Parser-Generating System for Constructing Compressed Compilers (11.444084)
1807: Optimization of Expressions in Fortran (10.668371)
1135: A General Business-Oriented Language Based on Decision Expressions* (10.559103)
1788: Toward a General Processor for Programming Languages (10.559103)
1988: A Formalism for Translator Interactions (10.360685)
2652: Reduction of Compilation Costs Through Language Contraction (10.360685)
```

### Discussion

From looking at the parameter calculation we can deduce the following:

- Setting the `lengthNorm` to a constant implies a penalty/advantage based on
  the number of terms in the field.
  Bigger fields will intrinsically have a higher value due to their size and not
  specificity; reducing thus the accuracy of the search.

- The new formulae for `idf` gives a slightly higher incentive to infrequent
  terms.

- The new formulae for `tf` asymptotically gives much less incentive for
  repeated terms within the same document.

From looking at the results:

We can witness that documents with longer values are ranked much higher with our
custom _Similarity_: For instance, document 1647 (_WATFOR-The University of
Waterloo FORTRAN IV Compiler_) ranked much lower in the original similarity than
document 3189 (_An Algebraic Compiler for the FORTRAN Assembly Program_) (which
didn't even make it top-10 in our similarity).
By looking at the original data, we observe the summary on document 1647 is
805-characters long whereas document 3189 is 392-characters long.
