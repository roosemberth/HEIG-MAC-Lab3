package ch.heigvd.iict.mac.labo1;

import java.io.IOException;
import java.util.function.Consumer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.Similarity;

import ch.heigvd.iict.mac.labo1.indexer.CACMIndexer;
import ch.heigvd.iict.mac.labo1.parsers.CACMParser;
import ch.heigvd.iict.mac.labo1.queries.QueriesPerformer;
import ch.heigvd.iict.mac.labo1.similarities.MySimilarity;

public class Main {

	public static void main(String[] args) {

		// 1.1. create an analyzer
		Analyzer analyser = getAnalyzer();

		Similarity similarity = new MySimilarity();

		CACMIndexer indexer = new CACMIndexer(analyser, similarity);
		long startTime = System.currentTimeMillis();
		indexer.openIndex();
		CACMParser parser = new CACMParser("documents/cacm.txt", indexer);
		parser.startParsing();
		indexer.finalizeIndex();
		long endTime = System.currentTimeMillis();
		System.out.printf("Processing time ms: %d\n", endTime - startTime);

		QueriesPerformer queriesPerformer = new QueriesPerformer(analyser, similarity);

		// Section "Reading Index"
		readingIndex(queriesPerformer);

		// Section "Searching"
		searching(queriesPerformer);

		queriesPerformer.close();

	}

	private static void readingIndex(QueriesPerformer queriesPerformer) {
		queriesPerformer.printTopRankingTerms("author", 10);
		queriesPerformer.printTopRankingTerms("title", 10);
	}

	private static void searching(QueriesPerformer queriesPerformer) {
		Consumer<TopDocs> displayResults = (TopDocs results) -> {
			long nhits = results.totalHits.value;
			System.out.println("Found " + Long.toString(nhits) + " hits.");
			for (ScoreDoc r : results.scoreDocs) {
				Document doc;
				try {
					doc = queriesPerformer.getIndexSearcher().doc(r.doc);
				} catch (IOException e) {
					throw new RuntimeException("Could not retrieve result document.", e);
				}
				var idBytes = doc.getField("id").binaryValue().bytes;
				String id = Long.toString(LongPoint.decodeDimension(idBytes, 0));
				String title = doc.get("title");
				String score = Float.toString(r.score);
				System.out.println(id + ": " + title + " (" + score + ")");
			}
		};

		displayResults.accept(queriesPerformer.query("compiler program"));

		displayResults.accept(queriesPerformer.query("\"Information Retrieval\""));
		displayResults.accept(queriesPerformer.query("Information AND Retrieval"));
		displayResults.accept(queriesPerformer.query("+Retrieval Information -Database"));
		displayResults.accept(queriesPerformer.query("Info*"));
		displayResults.accept(queriesPerformer.query("+\"Information Retrieval\"~5"));
	}

	private static Analyzer getAnalyzer() {
		return new EnglishAnalyzer();
	}

}
