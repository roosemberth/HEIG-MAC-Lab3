package ch.heigvd.iict.mac.labo1.similarities;

import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.search.similarities.ClassicSimilarity;

public class MySimilarity extends ClassicSimilarity {
	@Override
	public float tf(float freq) {
		Double res = 1 + Math.log(freq);
		return res.floatValue();
	}

	@Override
	public float lengthNorm(int numTerms) {
		return 1f;
	}

	@Override
	public Explanation idfExplain(CollectionStatistics collectionStats, TermStatistics termStats) {
		final long df = termStats.docFreq();
		final long docCount = collectionStats.docCount();
		final float idf = idf(df, docCount);
		return Explanation.match(idf, "idf, computed as log(numDocs/(docFreq+1)) + 1 from:",
				Explanation.match(df, "docFreq, number of documents containing term"),
				Explanation.match(docCount, "numDocs, number of documents with field"));
	}

	@Override
	public float idf(long docFreq, long docCount) {
		Double frac = (double) docCount / (double)(docFreq + 1);
		Double r = 1d + Math.log(frac);
		return r.floatValue();
	}
}
