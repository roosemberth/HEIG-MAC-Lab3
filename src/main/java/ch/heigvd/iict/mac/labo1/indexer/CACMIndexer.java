package ch.heigvd.iict.mac.labo1.indexer;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import ch.heigvd.iict.mac.labo1.parsers.ParserListener;

public class CACMIndexer implements ParserListener {

	private Directory dir = null;
	private IndexWriter indexWriter = null;

	private Analyzer analyzer = null;
	private Similarity similarity = null;

	public CACMIndexer(Analyzer analyzer, Similarity similarity) {
		this.analyzer = analyzer;
		this.similarity = similarity;
	}

	public void openIndex() {
		// 1.2. create an index writer config
		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		iwc.setOpenMode(OpenMode.CREATE); // create and replace existing index
		iwc.setUseCompoundFile(false); // not pack newly written segments in a compound file:
		// keep all segments of index separately on disk
		if (similarity != null)
			iwc.setSimilarity(similarity);
		// 1.3. create index writer
		Path path = FileSystems.getDefault().getPath("index");
		try {
			this.dir = FSDirectory.open(path);
			this.indexWriter = new IndexWriter(dir, iwc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onNewDocument(Long id, String authors, String title, String summary) {
		Document doc = new Document();

		// “Keep the publication id in the index and show it in the result set, but
		// do not enable queries in this field.”
		FieldType idType = new FieldType();
		idType.setDimensions(1, Long.BYTES);
		idType.setStored(true);
		idType.setTokenized(false);
		idType.setIndexOptions(IndexOptions.NONE);
		// Cannot store term vectors for a field that is not indexed.
		// idType.setStoreTermVectors(true);
		idType.freeze();
		doc.add(new Field("id", LongPoint.pack(id), idType));

		FieldType storedStringWithTermVector = new FieldType();
		storedStringWithTermVector.setOmitNorms(true);
		storedStringWithTermVector.setIndexOptions(IndexOptions.DOCS);
		storedStringWithTermVector.setStored(true);
		storedStringWithTermVector.setTokenized(false);
		storedStringWithTermVector.setStoreTermVectors(true);
		storedStringWithTermVector.freeze();

		if (authors != null && !authors.isBlank())
			for (String author : authors.split("; *")) {
				// We purposefully prevent tokenizing the author names since two authors
				// with the same first name or last name are unrelated.
				// We use a custom type to store term vectors.
				doc.add(new Field("author", author, storedStringWithTermVector));
			}

		FieldType storedTextWithTermVector = new FieldType();
		storedTextWithTermVector.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
		storedTextWithTermVector.setTokenized(true);
		storedTextWithTermVector.setStored(true);
		storedTextWithTermVector.setStoreTermVectors(true);
		storedTextWithTermVector.freeze();
		doc.add(new Field("title", title, storedTextWithTermVector));

		if (summary != null && !summary.isBlank())
			doc.add(new Field("summary", summary, storedTextWithTermVector));

		try {
			this.indexWriter.addDocument(doc);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void finalizeIndex() {
		if (this.indexWriter != null)
			try {
				this.indexWriter.close();
			} catch (IOException e) {
				/* BEST EFFORT */ }
		if (this.dir != null)
			try {
				this.dir.close();
			} catch (IOException e) {
				/* BEST EFFORT */ }
	}

}
