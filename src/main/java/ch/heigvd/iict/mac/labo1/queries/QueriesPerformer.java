package ch.heigvd.iict.mac.labo1.queries;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.misc.HighFreqTerms;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Stream;

public class QueriesPerformer {

	private Analyzer		analyzer		= null;
	private IndexReader 	indexReader 	= null;
	private IndexSearcher 	indexSearcher 	= null;

	public QueriesPerformer(Analyzer analyzer, Similarity similarity) {
		this.analyzer = analyzer;
		Path path = FileSystems.getDefault().getPath("index");
		Directory dir;
		try {
			dir = FSDirectory.open(path);
			this.indexReader = DirectoryReader.open(dir);
			this.indexSearcher = new IndexSearcher(indexReader);
			if(similarity != null)
				this.indexSearcher.setSimilarity(similarity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public IndexSearcher getIndexSearcher() {
		return this.indexSearcher;
	}

	public void printTopRankingTerms(String field, int numTerms) {
	  var comparator = new HighFreqTerms.TotalTermFreqComparator();
		try {
			var terms = HighFreqTerms.getHighFreqTerms(indexReader, 10, field, comparator);
			System.out.print("Top ranking terms for field ["  + field +"] are: ");
			System.out.println(Arrays.toString(Stream.of(terms)
						.map(t -> t.termtext.utf8ToString() + ": " + Integer.toString(t.docFreq))
						.toArray()));
		} catch (Exception e) {
			throw new RuntimeException("Could not calculate top ranking terms.", e);
		}
	}

	public TopDocs query(String q) {
		System.out.println("Searching for [" + q +"]");

		QueryParser parser = new QueryParser("summary", this.analyzer);
		Query query;
		try {
			query = parser.parse(q);
		} catch (Exception e) {
			throw new RuntimeException("Failed to parse query.", e);
		}
		try {
			return this.indexSearcher.search(query, 10);
		} catch (IOException e) {
			throw new RuntimeException("Could not execute search.", e);
		}
	}

	public void close() {
		if(this.indexReader != null)
			try { this.indexReader.close(); } catch(IOException e) { /* BEST EFFORT */ }
	}

}
